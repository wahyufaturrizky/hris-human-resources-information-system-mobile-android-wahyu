import React, { Component } from "react";
import axios from "axios";
import Config from "react-native-config";
import {SendBirdAction} from "../../store/SendBirdAction";
import { bindActionCreators } from "redux";

import moment from "moment";
import { connect } from "react-redux";
import { View, Text, ScrollView, FlatList, StyleSheet } from "react-native";
import { widthPercentageToDP } from "../../utils";
import Bubble from "../../components/Bubble";
import Compose from "../../components/Compose";
import { sendBirdFetchMessages } from "../../store/actions/sendBird";

const sb = SendBirdAction.getInstance();

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      messages: []
    };
    this.fetchMessages = this.fetchMessages.bind(this);
  }

  async fetchMessages () {
    try {
      const { navigation } = this.props;
      const channel = navigation.getParam('channel', '{}');
      let messages = await sb.getMessageList(channel, true);
      await this.setState({
        messages
      })
      if(channel.isGroupChannel()){
        await sb.markAsRead(channel)
      }
    } catch (e) {
      console.log(e)
    }
  }

  componentDidMount () {
    this.fetchMessages()
  }

  render() {
    const { navigation } = this.props;
    const channel = navigation.getParam('channel', '{}');
    return (
      <View style={styles.container}>
      {
        this.state.messages ? (
          <FlatList
            style={styles.container}
            data={this.state.messages}
            renderItem={Bubble}
            keyExtractor={(item, index) => (`message-${index}`)}
          />
        ) : null
      }
        <Compose channel={channel} fetchMessages={this.fetchMessages} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
      flex: 1,
      backgroundColor: 'white'
  },
  listItem: {
      width: '70%',
      margin: 10,
      padding: 10,
      backgroundColor: 'white',
      borderColor: '#979797',
      borderStyle: 'solid',
      borderWidth: 1,
      borderRadius: 10
  },
  incomingMessage: {
      alignSelf: 'flex-end',
      backgroundColor: '#E1FFC7'
  }
})

const mapStateToProps = ({Auth, SendBird}) => ({Auth, SendBird});

const mapDispatchToProps = dispatch => bindActionCreators({sendBirdFetchMessages}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Login);
