import React, { Component } from "react";
import { View, ScrollView, Text, Linking } from "react-native";
import RNPickerSelect from "react-native-picker-select";
import Toast from "react-native-easy-toast";
import { connect } from "react-redux";
import Axios from "axios";
import Config from "react-native-config";

import { axios, heightPercentageToDP } from "../../utils";
import Modal from "react-native-modal";

import styles from "./styles";
import { widthPercentageToDP } from "../../utils";
import { LabelForm, SAButton, InputForm } from "../../components/Form";

class PricingCalculator extends Component {
  constructor(props) {
    super(props);
    this.state = {
      notifModal: false,
      notifEmpty: false,
      brand: "",
      carModel: "",
      carType: "",
      carKM: "",
      carColor: "",
      carYear: "",
      kmSelectBox: null,
      brandList: [],
      carModelList: [],
      carTypeList: [],
      carYearList: [],
      carRequest: "",
      carYearRequest: "",
    };
    this.getPriceResult = this.getPriceResult.bind(this);
    this.notificationToggle = this.notificationToggle.bind(this);
    this.emptyNotifToggle = this.emptyNotifToggle.bind(this);
    this.checkPriceBtn = this.checkPriceBtn.bind(this);
    this.onPressCallOperator = this.onPressCallOperator.bind(this);
    this.onChangeRequestCar= this.onChangeRequestCar.bind(this);
    this.onChangeRequestYear = this.onChangeRequestYear.bind(this);
  }

  componentDidMount() {
    this.fetchBrand();
  }

  fetchBrand() {
    if (this.props.Auth.token ) {
      axios
        .get("/car/brand")
        .then(result => {
          this.setState({
            carModel: "",
            carType: "",
            carKM: "",
            carColor: "",
            carYear: "",
            carPrice: {},
            idResult: "",
            brandList: result.data.carBrand.map(car => ({ key: car.id, label: car.brand, value: car.id })),
            carModelList: [],
            carTypeList: [],
            carYearList: [],
          });
        })
        .catch(err => {
          console.log(err);
        });
    }
  }

  fetchType(id) {
    axios
      .get(`/car/brand/${id}`)
      .then(result => {
        this.setState({
          carType: "",
          carKM: "",
          carColor: "",
          carYear: "",
          carModelList: result.data.carType.map(data => ({ key: data.id, label: data.carType, value: data.id })),
          carTypeList: [],
          carYearList: [],
        });
      })
      .catch(err => {
        console.log(err);
      });
  }

  fetchYear(id) {
    axios
      .get(`/car/type/year/${id}`)
      .then(result => {
        this.setState({
          carType: "",
          carTypeList: [],
          carYear: result.data.yearList[0].year.toString(),
          carYearList: result.data.yearList.map(car => ({
            key: car.year.toString(),
            label: car.year.toString(),
            value: car.year.toString(),
          })),
        });
      })
      .catch(err => {
        console.log("ERROR YEAR");
        console.log(err);
      });
  }

  fetchSpec(year) {
    axios
      .get(`/car/spec/year/${this.state.carModel}/${year}`)
      .then(result => {
        this.setState({
          carTypeList: result.data.carSpec.map(car => ({ key: car.id, label: car.carSpec, value: car.id })),
        });
      })
      .catch(err => {
        console.log(err);
      });
  }

  onChangeBrand(id) {
    if (id !== "") {
      this.fetchType(id);
    }
    this.setState({ brand: id });
  }

  onChangeType(id) {
    if (id !== "") {
      this.fetchYear(id);
    }
    this.setState({ carModel: id });
  }

  onChangeYear(year) {
    if (year != "") {
      this.fetchSpec(year);
    }
    this.setState({ carYear: year });
  }

  onChangeSpec(id) {
    this.setState({ carType: id });
  }

  onChangeRequestCar(value) {
    this.setState({
      carRequest: value
    })
  }
  onChangeRequestYear(value) {
    this.setState({ carYearRequest: value})
  }

  async onPressCallOperator() {
    await Linking.openURL("https://wa.me/6281381120832");
  }

  getPriceResult() {
    this.setState(
      {
        notifModal: false,
      },
      () => {
        this.props.navigation.navigate("PricingCalculatorResult", {
          carBrandId: this.state.brand,
          carSpec: this.state.carType,
          carType: this.state.carModel,
          carYear: this.state.carYear,
        });
      },
    );
  }

  notificationToggle() {
    this.setState({
      notifModal: !this.state.notifModal,
    });
  }

  emptyNotifToggle() {
    this.setState({
      notifEmpty: !this.state.notifEmpty,
    });
  }

  checkPriceBtn() {
    const { brand, carType, carModel, carYear } = this.state;
    if (!(brand && carType && carModel && carYear)) {
      this.refs.toast.show("Semua data wajib di isi");
    } else {
      this.setState({
        notifModal: !this.state.notifModal,
      });
    }
  }

  render() {
    let { brandList, carModelList, carTypeList, carYearList } = this.state;
    return (
      <View style={styles.container}>
        <Toast position="top" positionValue={10} opacity={0.75} ref="toast" />
        <Notification
          notifModal={this.state.notifModal}
          notificationToggle={this.notificationToggle}
          getPriceResult={this.getPriceResult}
        />
        <EmptyNotification
          notifEmpty={this.state.notifEmpty}
          emptyNotifToggle={this.emptyNotifToggle}
          carRequest={this.state.carRequest}
          carYearRequest={this.state.carYearRequest}
          onChangeRequestCar={this.onChangeRequestCar}
          onChangeRequestYear={this.onChangeRequestYear}
          onPressCallOperator={this.onPressCallOperator}
        />

        <ScrollView style={styles.ScrollViewStyle}>
          <LabelForm value="Brand" style={{ color: "#005596" }} />
          <View style={styles.borderStyle}>
            <RNPickerSelect
              style={{ inputIOS: styles.input, inputAndroid: styles.input }}
              placeholder={{ label: "Select Brand", value: "" }}
              onValueChange={v => this.onChangeBrand(v)}
              items={brandList}
              value={this.state.brand}
            />
          </View>

          <LabelForm value="Model" style={{ color: "#005596" }} />
          <View style={styles.borderStyle}>
            <RNPickerSelect
              style={{ inputAndroid: styles.input, inputIOS: styles.input }}
              placeholder={{ label: "Select Model", value: "" }}
              onValueChange={v => this.onChangeType(v)}
              items={carModelList}
              value={this.state.carModel}
            />
          </View>

          <LabelForm value="Tahun" style={{ color: "#005596" }} />
          <View style={styles.borderStyle}>
            <RNPickerSelect
              style={{ inputAndroid: styles.input, inputIOS: styles.input }}
              onValueChange={v => this.onChangeYear(v)}
              items={carYearList}
              value={this.state.carYear}
            />
          </View>

          <LabelForm value="Tipe" style={{ color: "#005596" }} />
          <View style={styles.borderStyle}>
            <RNPickerSelect
              style={{ inputAndroid: styles.input, inputIOS: styles.input }}
              placeholder={{ label: "Select Tipe", value: "" }}
              onValueChange={v => this.onChangeSpec(v)}
              items={carTypeList}
              value={this.state.carType}
            />
          </View>

          <View style={styles.buttonArea}>
            <SAButton
              style={{
                width: widthPercentageToDP("43%"),
                height: 60,
                elevation: 3,
                shadowOffset: { width: 10, height: 10 },
                shadowColor: "#FB8B34",
                shadowRadius: 18,
                shadowOpacity: 0.5,
              }}
              fontSize={18}
              backgroundColor={"#FB8B34"}
              title="Cek Harga"
              onPress={this.checkPriceBtn}
            />
            <SAButton
              style={{
                width: widthPercentageToDP("43%"),
                height: 60,
                elevation: 3,
                shadowOffset: { width: 10, height: 10 },
                shadowColor: "#FB3456",
                shadowRadius: 18,
                shadowOpacity: 0.5,
              }}
              backgroundColor={"#FB3434"}
              title="Mobil Tidak Ada di List"
              onPress={this.emptyNotifToggle}
            />
          </View>
        </ScrollView>
      </View>
    );
  }
}

const EmptyNotification = props => {
  return (
    <Modal isVisible={props.notifEmpty} onBackdropPress={() => props.emptyNotifToggle()}>
      <View
        style={{
          backgroundColor: "white",
          justifyContent: "center",
          alignItems: "center",
          borderRadius: 4,
          borderColor: "rgba(0, 0, 0, 0.1)",
          marginHorizontal: widthPercentageToDP("4%"),
        }}
      >
        <View style={{ paddingVertical: 15, paddingHorizontal: 25 }}>
          <View>
            <View style={{ alignItems: "center", marginBottom: heightPercentageToDP("3%") }}>
              <Text style={{ fontSize: 20, color: "#005596", fontWeight: "600" }}>Mobil Tidak Ada di List</Text>
              <View style={styles.borderTitle} />
            </View>

            <Text
              style={{
                marginBottom: heightPercentageToDP("2.5%"),
                fontFamily: "NunitoSans-SemiBold",
                textAlign: "center",
                fontSize: 16,
              }}
            >
              Mohon tuliskan mobil yang anda cari
            </Text>
            <InputForm
              editable={true}
              value={props.carRequest}
              placeholder="Ex : Avanza All New Veloz G/AT"
              onChangeText={(e) => props.onChangeRequestCar(e)}
              style={{ borderColor: "#DCE7EF", borderWidth: 1, marginTop: widthPercentageToDP("2%") }}
            />

            <InputForm
              editable={true}
              value={props.carYearRequest}
              placeholder="Year"
              keyboardType={"number-pad"}
              onChangeText={(e) => props.onChangeRequestYear(e)}
              style={{ borderColor: "#DCE7EF", borderWidth: 1, marginTop: widthPercentageToDP("2%") }}
            />

            <SAButton backgroundColor={"#FB8B34"} title="Submit" onPress={props.onPressCallOperator} />
          </View>
        </View>
      </View>
    </Modal>
  )
}

const Notification = props => {
  return (
    <Modal isVisible={props.notifModal} onBackdropPress={() => props.notificationToggle()}>
      <View
        style={{
          backgroundColor: "white",
          justifyContent: "center",
          alignItems: "center",
          borderRadius: 4,
          borderColor: "rgba(0, 0, 0, 0.1)",
          marginHorizontal: widthPercentageToDP("4%"),
        }}
      >
        <View style={{ paddingVertical: 15, paddingHorizontal: 25 }}>
          <View>
            <View style={{ alignItems: "center", marginBottom: heightPercentageToDP("3%") }}>
              <Text style={{ fontSize: 20, color: "#005596", fontWeight: "600" }}>Keterangan</Text>
              <View style={styles.borderTitle} />
            </View>

            <Text
              style={{
                marginBottom: heightPercentageToDP("2.5%"),
                fontFamily: "NunitoSans-SemiBold",
                textAlign: "center",
                fontSize: 16,
              }}
            >
              Harga yang diberikan merupakan{" "}
              <Text style={{ fontFamily: "NunitoSans-Black", color: "#FB3434" }}>estimasi awal</Text> yang dpat
              ditawarkan Toyota Trust dan{" "}
              <Text style={{ fontFamily: "NunitoSans-Black", color: "#FB3434" }}>
                bukan merupakan harga final setelah proses appraisal
              </Text>
            </Text>

            <SAButton backgroundColor={"#FB8B34"} title="Cek Harga" onPress={props.getPriceResult} />
          </View>
        </View>
      </View>
    </Modal>
  );
};

const mapStateToProps = ({ Auth }) => ({ Auth })
export default connect(mapStateToProps)(PricingCalculator);
