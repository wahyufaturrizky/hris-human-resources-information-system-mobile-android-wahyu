import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { View, Text, TouchableOpacity, ScrollView } from "react-native";

import { widthPercentageToDP } from "../../utils";

class Inbox extends Component {
  constructor(props) {
    super(props);
    this.state = {
      messages: [],
    }
  }


  componentDidMount = async () => {
    try {
    } catch (e) {
      console.log(e)
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <Text> Tidak ada Inbox</Text>
      </View>
    );
  }
}

const styles = {
  container: {
    height: '100%',
    backgroundColor: "white",
  },
  listContainer: {
    paddingBottom: widthPercentageToDP("4%"),
  },
  lastMessage: {
    fontFamily: "NunitoSans-SemiBold",
    fontSize: widthPercentageToDP("3.7%"),
    color: "#005596"
  },
  bookingContainer: {
    marginLeft: widthPercentageToDP("4%"),
    borderBottomWidth: 1,
    borderColor: "#DCE7EF",
  },
  inboxName: {
    fontFamily: "NunitoSans-SemiBold",
    fontSize: widthPercentageToDP("6%"),
    color: "#172344"
  },
  itemContainer: {
    paddingVertical: widthPercentageToDP("4%"),
    paddingRight: widthPercentageToDP("4%"),
  },
  badge: {
    backgroundColor: "#FB8B34",
    fontSize: widthPercentageToDP("3.7%"),
    color: "white",
    fontFamily: "NunitoSans-SemiBold",
    borderRadius: 50,
    textAlign: "center",
    paddingHorizontal: 10,
    paddingVertical: 5
  }
};

let mapStateToProps = ({ Auth }) => ({ Auth })
let mapDispatchToProps = dispatch => bindActionCreators({sendBirdConnect}, dispatch);


export default connect(mapStateToProps, mapDispatchToProps)(Inbox);
