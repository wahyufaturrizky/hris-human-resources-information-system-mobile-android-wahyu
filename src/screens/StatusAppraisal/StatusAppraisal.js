import React, { Component } from "react";
import axios from "axios";
import Config from "react-native-config";

import moment from "moment";
import { connect } from "react-redux";
import { View, Text, TouchableOpacity, ScrollView, ActivityIndicator, SafeAreaView } from "react-native";
import { widthPercentageToDP } from "../../utils";

class StatusAppraisal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      bookingList: [],
      onLoadBooking: false
    };
  }

  componentDidMount() {
    this.fetchBookingList();
  }

  fetchBookingList() {
    const { Auth } = this.props;
    this.setState({ onLoadBooking: true })
    axios
      .get(`${Config.API_URL}/user/${Auth.user.id}/booking`, { headers: { authorization: Auth.token } })
      .then(result => {
        this.setState({
          bookingList: result.data.bookingList,
          onLoadBooking: false
        });
      })
      .catch(err => {
        this.setState({
          onLoadBooking: false
        });
        console.log(err);
      });
  }

  _onPressBooking(booking) {
    this.props.navigation.navigate("AppraisalDetail", { booking });
  }

  componentDidUpdate(prevProps) {
    if (prevProps.route.params?.booking !== this.props.route.params?.booking) {
      this.fetchBookingList();
    }
  }

  render() {
    const { bookingList, onLoadBooking } = this.state;
    return (
      <SafeAreaView style={styles.container}>
        {onLoadBooking ? (
          <ActivityIndicator animating={this.state.onProgress} size="large" color="#0000ff" />
        ) : (
          <ScrollView style={styles.listContainer} >
            {bookingList.length > 0
              ? bookingList.map(booking => (
                  <TouchableOpacity
                    style={styles.bookingContainer}
                    key={booking.id}
                    onPress={() => this._onPressBooking(booking)}
                  >
                    <View style={styles.itemContainer}>
                      <View style={{flexDirection: "row", justifyContent: "space-between"}}>
                        <Text style={styles.bookingId}>{booking.noBooking}</Text>
                        <Text style={styles.bookingDate}>
                          {moment(booking.bookingTime).format("dddd, Do MMM YYYY")}
                        </Text>
                      </View>
                      <Text style={styles.carType}>{booking.carType}</Text>
                      <View style={{flexDirection: "row", justifyContent: "space-between"}}>
                        <Text style={styles.licensePlate}>{booking.Appraisal ? booking.Appraisal.carDetail[7].value : null}</Text>
                        <Text style={styles.bookingStatus}>
                          {`${booking.BookingStatuses[0].status_label} - ${booking.BookingStatuses[0].status}`}
                        </Text>
                      </View>
                    </View>
                  </TouchableOpacity>
                ))
              : null}
          </ScrollView>
        )}
      </SafeAreaView>
    );
  }
}

const styles = {
  container: {
    height: '100%',
    backgroundColor: "white",
  },
  headerText: {
    color: "#0057A0",
    fontSize: 20,
    fontWeight: "bold",
  },
  listContainer: {
    paddingBottom: widthPercentageToDP("4%"),
  },
  bookingId: {
    fontFamily: "NunitoSans-SemiBold",
    fontSize: widthPercentageToDP("4%"),
    fontWeight: "600",
    color: "#005596",
  },
  bookingDate: {
    fontFamily: "NunitoSans-SemiBold",
    fontSize: widthPercentageToDP("3.7%"),
    color: "#005596"
  },
  bookingStatus: {
    fontFamily: "NunitoSans-SemiBold",
    fontSize: widthPercentageToDP("3.7%"),
    color: "#FB8B34"
  },
  carType: {
    fontFamily: "NunitoSans-SemiBold",
    fontSize: widthPercentageToDP("5.4%"),
    color: "#172344"
  },
  licensePlate: {
    fontFamily: "NunitoSans-Regular",
    fontSize: widthPercentageToDP("4.2%"),
    color: "#172344"
  },
  bookingContainer: {
    marginLeft: widthPercentageToDP("4%"),
    borderBottomWidth: 1,
    borderColor: "#DCE7EF",
  },
  itemContainer: {
    paddingVertical: widthPercentageToDP("4%"),
    paddingRight: widthPercentageToDP("4%"),
  }
};

const mapStateToProps = ({ Auth }) => ({ Auth });

export default connect(mapStateToProps)(StatusAppraisal);
