import React, { Component } from "react";
import axios from "axios";
import Config from "react-native-config";
import { connect } from "react-redux";
import Modal from "react-native-modal";
import { Text, View, Image, StyleSheet, TouchableOpacity, Linking, Platform, ScrollView , Dimensions} from "react-native";
import Carousel from 'react-native-banner-carousel';
import { Header } from "react-native-elements";
import Slider from 'react-native-slider';

import Button from "../../components/Button/Button";
import TrustIcon from "../../components/TrustIcon/TrustIcon";
import { version as appVersion } from "../../../package.json";
import { widthPercentageToDP, heightPercentageToDP, versionCompare } from "../../utils";
import { Value } from "react-native-reanimated";
import RNPicker from "../../components/RNPickerSearch";
import { LabelForm, SAButton, InputForm, SAGradientButton } from "../../components/Form";
import NumberFormat from 'react-number-format';
import {RFValue} from 'react-native-responsive-fontsize';
import ComponentMonthPeriod from '../../components/ComponentMonthPeriod/ComponentMonthPeriod';
import ComponentDayPeriod from '../../components/ComponentDayPeriod/ComponentDayPeriod';
import LinearGradient from "react-native-linear-gradient";
const BannerWidth = Dimensions.get('window').width;
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

class Homepage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalUpdate: false,
      modalLogout: false,
      sliderImages: [],
      value_peminjaman: null,
      value_hari: null,
      value_bulan: null,
      selectedMonth: false,
      selectedDay: false,
    };
    this.navigationWillFocusListener = props.navigation.addListener("willFocus", () => {
      this.checkAppVersion();
      this.checkSalesProfile();
    });
    this.renderPage = this.renderPage.bind(this);
  }

  componentDidUpdate(prevProps) {
    const { SalesProfile } = this.props;
    if (prevProps.SalesProfile.onRequest) {
      if (!SalesProfile.onRequest) {
        if (!SalesProfile.isUpdated) {
          this.props.navigation.navigate("Profil");
        }
      }
    }
  }

  componentDidMount() {
    axios
      .get(`${Config.API_URL}/banner`)
      .then((result) => {
        this.setState({
          sliderImages: result.data.banner
        })
      })
  }

  onPressButton(value) {
    this.props.navigation.navigate(value);
  }

  checkSalesProfile() {
    const { SalesProfile } = this.props;
    if (!SalesProfile.onRequest) {
      if (!SalesProfile.isUpdated) {
        this.props.navigation.navigate("Profil");
      }
    }
  }

  checkAppVersion() {
    axios
      .get(`${Config.API_URL}/com.trustsalesapp`)
      .then(result => {
        const { latestVersion } = result.data;
        if (versionCompare(appVersion, latestVersion) < 0) {
          this.setState({
            modalUpdate: true,
          });
        }
      })
      .catch(error => {
        console.log("error can't get version");
      });
  }

  _onPressLogoutBtn = () => {
    this.setState({
      modalLogout: true,
    });
  };

  openAppLink = () => {
    if (Platform.OS === "android") {
      Linking.openURL("market://details?id=com.trustsalesapp");
    } else if (Platform.OS === "ios") {
      Linking.openURL("https://itunes.apple.com/us/app/auto-trust/id1453700704?mt=8");
    } else {
      console.log("Platform not supported");
    }
  };

  onPressModalLogoutYes() {
    this.setState({ modalLogout: false }, () => {
      this.props.navigation.navigate("Logout");
    });
  }

  onPressModalLogoutNo() {
    this.setState({
      modalLogout: !this.state.modalLogout,
    });
  }

  renderPage(image, index) {
    return (
      <View key={index}>
        <Image style={styles.bannerImage} source={{uri: image}}/>
      </View>
    )
  }

  handleChange = (value, name) => {
    console.log("Yang diketik di", name, " =", value)
    this.setState({
      [name]: value,
    })
  }

  render() {
    const { sliderImages } = this.state;
    return (
      <View style={styles.container}>
          <Header
            containerStyle={{height: heightPercentageToDP("10%")}}
            backgroundImage={require("../../assets/images/header_03.png")}
            leftComponent={
              // [START LOGO HEADER]
              // <Image
              //   style={{ width: widthPercentageToDP("15%"), height: widthPercentageToDP("15%"), flex: 1, marginBottom: heightPercentageToDP("15%") }}
              //   resizeMode="contain"
              //   source={require("../../assets/images/modalmu_logo.png")}
              // />
              // [END LOGO HEADER]

              <TouchableOpacity style={styles.modalmuHeader}>
                <Text style={styles.leftTextComponentHeader}>PT. Wahyu</Text>
              </TouchableOpacity>
            }
            rightComponent={
              <TouchableOpacity onPress={this._onPressLogoutBtn} style={styles.logoutBtn}>
                <Text style={styles.rightTextComponentHeader}>Logout</Text>
              </TouchableOpacity>
            }
          />
          <Modal isVisible={this.state.modalLogout}>
            <View style={styles.modalLogout}>
              <Text style={styles.modalLogoutTitle}>Logout</Text>
              <View style={{ paddingTop: 10, paddingBottom: 10 }}>
                <Text style={styles.modalLogoutDesc}>Apakah anda yakin ingin logout?</Text>
              </View>
              <View style={{ flexDirection: "row", justifyContent: "space-around", width: "100%" }}>
                <Button onPress={() => this.onPressModalLogoutNo()} text="Tidak" />
                <Button
                  onPress={() => this.onPressModalLogoutYes()}
                  text="Ya"
                  buttonStyle={{ backgroundColor: "#FB8B34" }}
                />
              </View>
            </View>
          </Modal>
          <Modal isVisible={this.state.modalUpdate}>
            <View
              style={{
                backgroundColor: "white",
                justifyContent: "center",
                alignItems: "center",
                borderRadius: 4,
                borderColor: "rgba(0, 0, 0, 0.1)",
                marginHorizontal: 10,
              }}
            >
              <View style={{ padding: 10 }}>
                <View style={{ alignItems: "center" }}>
                  <Text style={{ fontWeight: "600" }}>Update Applikasi Anda</Text>
                </View>
                <View style={{ marginTop: 20 }}>
                  <Text style={{ textAlign: "justify" }}>
                    Halo Bapak/Ibu wiraniaga AUTO2000! Aplikasi yg Bapak/Ibu gunakan merupakan versi lama. Mohon update ke
                    versi baru aplikasi Bapak/Ibu dengan menekan tombol dibawah ini.
                  </Text>
                  <View style={{ alignItems: "center", marginTop: 20 }}>
                    <Button
                      onPress={() => this.openAppLink()}
                      text="Update"
                      buttonStyle={{ backgroundColor: "#0057A0" }}
                    />
                  </View>
                </View>
              </View>
            </View>
          </Modal>

            <ScrollView>
              {/* <View style={styles.banner}>
              {sliderImages.length > 0 ? (
                <Carousel
                  autoplay
                  autoplayTimeout={5000}
                  loop
                  index={0}
                  pageSize={BannerWidth}
                >
                  {sliderImages.map((image, idx) => this.renderPage(image, idx))}
                  <Image source={require("../../assets/images/banner_01.png")} style={styles.bannerImage} />
                  <Image source={require("../../assets/images/banner_02.png")} style={styles.bannerImage} />
                  <Image source={require("../../assets/images/banner_03.png")} style={styles.bannerImage} />
                </Carousel>
              ) : (
                <Image source={require("../../assets/images/banner_03.png")} style={styles.bannerImage} />
              )}
              </View> */}

              <LinearGradient colors={['#46BCFF', '#3A7BD5']}>
                {/* [START NAME AND PROFIL] */}
                <View 
                  style={{
                    flexDirection: 'row',
                    width: '100%',
                    paddingHorizontal: widthPercentageToDP("4%"),
                    marginTop: RFValue(24),
                    justifyContent: 'space-between',
                  }}
                >
                  <View>
                    <Text style={styles.styleHeaderNamaProfil}>Wahyu Fatur Rizki</Text>
                    <Text style={styles.styleSubHeaderNamaProfil}>Software Engineer</Text>
                  </View>
                  <View>
                    <TouchableOpacity>
                      <Image source={require("../../assets/images/foto_wahyu.png")} style={styles.imagePhotoProfil} />
                    </TouchableOpacity>
                  </View>
  
                </View>
                {/* [END NAME AND PROFIL] */}
      
                <View style={styles.menuBox}>
                  
                  <View style={styles.menu}>
                    <TouchableOpacity onPress={() => this.onPressButton("UnderConstruction")}>
                      <View style={styles.box}>
                        <TrustIcon name="calculator_icon" width={36} height={47} />
                      </View>
                    </TouchableOpacity>
                    <Text style={styles.menuTitle}>Rembeursment</Text>
                  </View>
      
                  <View style={styles.menu}>
                    <TouchableOpacity onPress={() => this.onPressButton("LiveAttendance")}>
                      <View style={styles.box}>
                        <TrustIcon name="booking_icon" width={40} height={47} />
                      </View>
                    </TouchableOpacity>
                    <Text style={styles.menuTitle}>Live Attendance</Text>
                  </View>
      
                  <View style={styles.menu}>
                    <TouchableOpacity onPress={() => this.onPressButton("UnderConstruction")}>
                      <View style={styles.box}>
                        <TrustIcon name="tracking_icon" width={49} height={39} />
                      </View>
                    </TouchableOpacity>
                    <Text style={styles.menuTitle}>Calendar</Text>
                  </View>
      
                  <View style={styles.menu}>
                    <TouchableOpacity onPress={() => this.onPressButton("UnderConstruction")}>
                      <View style={styles.box}>
                        <TrustIcon name="report_icon" width={47} height={47} />
                      </View>
                    </TouchableOpacity>
                    <Text style={styles.menuTitle}>Attendance</Text>
                  </View>
      
                  <View style={styles.menu}>
                    <TouchableOpacity onPress={() => this.onPressButton("UnderConstruction")}>
                      <View style={styles.box}>
                        <TrustIcon name="news_icon" width={47} height={43} />
                      </View>
                    </TouchableOpacity>
                    <Text style={styles.menuTitle}>My Payslip</Text>
                  </View>
      
                  <View style={styles.menu}>
                    <TouchableOpacity onPress={() => this.onPressButton("UnderConstruction")}>
                      <View style={styles.box}>
                        <TrustIcon name="more_icon" width={47} height={13} />
                      </View>
                    </TouchableOpacity>
                    <Text style={styles.menuTitle}>More</Text>
                  </View>
                </View>

              </LinearGradient>

              {/* [START Announcement] */}
              <View 
                  style={{
                    flexDirection: 'row',
                    width: '100%',
                    paddingHorizontal: widthPercentageToDP("4%"),
                    marginTop: RFValue(24),
                    justifyContent: 'space-between',
                  }}
                >
                  <View>
                    <Text style={styles.styleTextAnnouncement}>Announcement</Text>
                  </View>
                  <View>
                    <Text style={styles.styleTextViewAll}>View All</Text>
                  </View>
  
                </View>
                
                {/* [START LIST ANNOUNCEMENT] */}
                <View 
                    style={{
                      flexDirection: 'row',
                      width: '100%',
                      paddingHorizontal: widthPercentageToDP("4%"),
                      marginTop: RFValue(24),
                      justifyContent: 'space-between',
                      paddingBottom: heightPercentageToDP('5%'),
                      borderBottomWidth: 2,
                      borderBottomColor: '#BDBDBD',
                    }}
                  >
                    <View style={{width: widthPercentageToDP('55%'),}}>
                      <Text style={styles.styleTextAnnouncementTitle}>Notice of Sallary Delay to 06th Jully 2020</Text>
                    </View>
                    
                    <View>
                      <Text style={styles.styleTextDate}>25 Jun 2020</Text>
                    </View>
    
                  </View>
                  <View 
                      style={{
                        flexDirection: 'row',
                        width: '100%',
                        paddingHorizontal: widthPercentageToDP("4%"),
                        marginTop: RFValue(24),
                        justifyContent: 'space-between',
                        paddingBottom: heightPercentageToDP('5%'),
                        borderBottomWidth: 2,
                        borderBottomColor: '#BDBDBD'
                      }}
                    >
                      <View style={{width: widthPercentageToDP('55%'),}}>
                        <Text style={styles.styleTextAnnouncementTitle}>Notice of Sallary Delay to 06th Jully 2020</Text>
                      </View>
                      <View>
                        <Text style={styles.styleTextDate}>25 Jun 2020</Text>
                      </View>
      
                    </View>
                    {/* [END LIST ANNOUNCEMENT] */}

                {/* [END Announcement] */}
              
            </ScrollView>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  styleTextAnnouncement: {
    fontSize: RFValue(18),
    color: '#333333',
    fontWeight: 'bold'
  },
  styleTextViewAll: {
    fontSize: RFValue(18),
    color: '#005596'
  },
  styleTextAnnouncementTitle: {
    fontSize: RFValue(18),
    color: '#333333',
  },
  styleTextDate: {
    fontSize: RFValue(18),
    color: '#828282'
  },
  imagePhotoProfil : {
    width: widthPercentageToDP('16%'),
    height: heightPercentageToDP('10%'),
  },
  styleHeaderNamaProfil : {
    fontSize: RFValue(28),
    color: 'white',
    fontWeight: 'bold'
  },
  styleSubHeaderNamaProfil : {
    fontSize: RFValue(18),
    color: 'white'
  },
  container: {
    height: "100%",
    backgroundColor: "white",
    paddingBottom: widthPercentageToDP("4%"),
  },
  banner: {
    margin: widthPercentageToDP("4%"),
  },
  bannerImage: {
    width: widthPercentageToDP("92%"),
    height: widthPercentageToDP("50%"),
    borderTopLeftRadius: 8,
    borderTopRightRadius: 8,
    borderBottomLeftRadius: 8,
    borderBottomRightRadius: 8,
  },
  menuBox: {
    flexDirection: "row",
    flexWrap: "wrap",
    paddingHorizontal: widthPercentageToDP("4%"),
    backgroundColor: 'white',
    marginHorizontal: widthPercentageToDP('4%'),
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    marginVertical: heightPercentageToDP('4%'),
    shadowColor: '#000',
    shadowOffset: {
      width: widthPercentageToDP('0%'),
      height: heightPercentageToDP('8%')
    },
    shadowOpacity: 0.44,
    shadowRadius: 10.32,
    elevation: 12,
  },
  menu: {
    margin: widthPercentageToDP("2%"),
    alignItems: "center",
  },
  menuTitle: {
    fontFamily: "NunitoSans-SemiBold",
    color: "#005596",
    fontSize: RFValue(14),
  },
  menuTitleJumlahPeminjaman: {
    fontFamily: "NunitoSans-SemiBold",
    color: "#666666",
    fontWeight: "300",
    fontSize: RFValue(18),
    marginTop: RFValue(12)
  },
  menuTitlePeriodeHari: {
    fontFamily: "NunitoSans-SemiBold",
    color: "#666666",
    fontWeight: "300",
    fontSize: RFValue(18),
    marginTop: RFValue(12)
  },
  menuTitleJumlahPeminjamanInitial: {
    fontFamily: "NunitoSans-SemiBold",
    color: "#FF9D2E",
    fontWeight: "bold",
    fontSize: RFValue(35),
  },
  box: {
    width: widthPercentageToDP("22,66%"),
    height: widthPercentageToDP("22,66%"),
    paddingVertical: widthPercentageToDP("4%"),
    borderColor: "#DCE7EF",
    borderWidth: 1,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    alignItems: "center",
    justifyContent: "center",
  },
  modalLogout: {
    backgroundColor: "white",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 4,
    borderColor: "rgba(0, 0, 0, 0.1)",
    marginHorizontal: 10,
    padding: 20,
  },
  modalLogoutTitle: {
    color: "#005596",
    fontSize: RFValue(20),
    fontFamily: "Nunito Sans",
  },
  modalLogoutDesc: {
    fontFamily: "Nunito Sans",
  },
  rightTextComponentHeader: {
    fontSize: RFValue(16),
    color: "white",
  },
  leftTextComponentHeader: {
    fontSize: RFValue(16),
    color: "white",
  },
  logoutBtn: {
    padding: widthPercentageToDP("3%"),
    marginTop: heightPercentageToDP("10%"),
    marginBottom: heightPercentageToDP("15%")
  },
  modalmuHeader: {
    padding: widthPercentageToDP("1%"),
    marginTop: heightPercentageToDP("10%"),
    marginBottom: heightPercentageToDP("15%")
  },
  styleIndicator: {
    color: '#9C9C9C',
    fontSize: RFValue(14),
  },
  styleLabelTujuanMeminjam: {
    marginLeft: widthPercentageToDP('2%'),
    color: '#666666',
    fontSize: RFValue(14),
  },
  styleButtonTujuanMeminjam: {
    color: '#666666',
    fontSize: RFValue(14),
  },
  styleLabelPilihanPaket: {
    marginLeft: widthPercentageToDP('2%'),
    color: '#666666',
    fontSize: RFValue(14),
  },
  styleButtonTujuanPilihanPaket: {
    color: '#45B7FB',
    fontSize: RFValue(14),
  },
});

const mapStateToProps = ({ Auth, SalesProfile }) => ({
  Auth,
  SalesProfile,
});

export default connect(mapStateToProps)(Homepage);
