import React, { Component } from "react";
import axios from "axios";
import moment from "moment";
import Config from "react-native-config";
import Modal from "react-native-modal";
// import MapView from "react-native-maps";
import DateTimePicker from '@react-native-community/datetimepicker';
import Permissions from "react-native-permissions";
import ImagePicker from "react-native-image-picker";
import Toast from "react-native-easy-toast";
import "moment/locale/id";

import { View, Text, ScrollView, Dimensions, SafeAreaView,Platform , Button, TouchableOpacity} from "react-native";
import { SAButton, LabelForm, InputForm } from "../../components/Form/Form";
import { GooglePlacesAutocomplete } from "react-native-google-places-autocomplete";
import { connect } from "react-redux";
import { widthPercentageToDP, heightPercentageToDP } from "../../utils";
import TrustIcon from "../../components/TrustIcon/TrustIcon";
import { RFValue } from "react-native-responsive-fontsize";
import LinearGradient from "react-native-linear-gradient";

moment.locale('id')

class ClockIn extends Component {
  constructor(props) {
    super(props);

    this.state = {
      latMaps: -6.17545698,
      lngMaps: 106.82691335,
      bookingTime: moment().locale("id"),
      yearNow: new Date().getFullYear(),
      carType: "",
      notes: "",
      onRequest: false,
      notifModal: false,
      mode: 'date',
      show: false,
      photo_selfie: '',
    };
    this._onChangeCarType = this._onChangeCarType.bind(this);
    this._onChangeNotes = this._onChangeNotes.bind(this);
    this._onPressSubmitData = this._onPressSubmitData.bind(this);
    this.notificationToggle = this.notificationToggle.bind(this);
    this.selectImageToUpload = this.selectImageToUpload.bind(this);
  }

  componentDidMount() {
  }

  _requestPermission = type => {
    Permissions.request(type).then(response => {
      this.setState({
        [`${type}Permission`]: response,
      });
    });
  };

  _onPressSubmitData() {
    const { state } = this;
    const { Auth } = this.props;
    if (!(state.carType && state.notes)) {
      this.refs.toast.show("Semua data wajib di isi");
    } else {
      this.setState(
        {
          onRequest: true,
        },
        () => {
          axios
            .post(
              `${Config.API_URL}/user/${Auth.user.id}/booking`,
              {
                locationLat: state.latMaps,
                locationLng: state.lngMaps,
                bookingTime: state.bookingTime,
                notes: state.notes,
                carType: state.carType,
              },
              {
                headers: { authorization: Auth.token },
              },
            )
            .then(result => {
              this.setState({
                notifModal: true,
              });
            })
            .catch(err => {
              console.log(err);
              this.refs.toast.show("Gagal Booking");
            })
            .finally(() => {
              this.setState({
                onRequest: false,
              });
            });
        },
      );
    }
  }

  _onChangeCarType(value) {
    this.setState({
      carType: value,
    });
  }

  _onChangeNotes(value) {
    this.setState({
      notes: value,
    });
  }

  _changeMarkerByPressMap(coor) {
    this.setState({
      latMaps: coor.coordinate.latitude,
      lngMaps: coor.coordinate.longitude,
    });
  }

  _changeMarkerByAddress(coor) {
    this.map.animateToCoordinate({ latitude: coor.lat, longitude: coor.lng }, 200);
    this.setState({ latMaps: coor.lat, lngMaps: coor.lng });
  }

  _onSelectAddress(place) {
    axios
      .get(
        `https://maps.googleapis.com/maps/api/place/details/json?key=AIzaSyAu5x9OygXYAqRoKC4MF8CUJsGfumPsRUw&placeid=${
          place.place_id
        }&fields=geometry`,
      )
      .then(res => {
        const { result } = res.data;
        this._changeMarkerByAddress(result.geometry.location);
      })
      .catch(err => {
        console.log(err);
      });
  }

  notificationToggle() {
    this.setState(
      {
        notifModal: !this.state.notifModal,
      },
      () => {
        this.props.navigation.navigate("Homepage");
      },
    );
  }

  onChangeDate = (event, selectedDate) => {
    const currentDate = selectedDate || date;
    console.log(this.state.mode, "lili")
    let mode={}
    if(this.state.mode === "date") {
      mode = {
        mode: "time",
        show: true
      }
    }
    this.setState({
      bookingTime: currentDate,
      show: false,
      ...mode
    });
  };

  showDatePicker = currentMode => {
    this.setState({
      show: true,
      mode: currentMode
    });
  };

  selectImageToUpload(field) {
    const options = {
      title: `Pilih ${field}`,
      storageOptions: {
        path: "images",
      },
    };
    ImagePicker.showImagePicker(options, response => {
      if (response.didCancel) {
        console.log("Pick image cancelled");
      } else if (response.error) {
        console.log("Error pick image", response.error);
      } else {
        this.setState({
          [field]: {
            uri: response.uri,
            type: response.type,
            name: field,
          },
          [`preview${field}`]: "data:image/jpeg;base64," + response.data,
        });
      }
    })
  }

  onPressButton(value) {
    this.props.navigation.navigate(value);
  }

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <Notification notifModal={this.state.notifModal} notificationToggle={this.notificationToggle} />
        <Toast position="top" positionValue={10} opacity={0.75} ref="toast" />
        <ScrollView>

          <LinearGradient colors={['#46BCFF', '#3A7BD5']}>

            <View style={{ padding: widthPercentageToDP("4%") }}>

              <View style={{ marginBottom: 15 }}>
                <Text style={styles.title}>Data Clock In</Text>
                <View style={styles.borderTitle} />
              </View>
            
            <LabelForm value="Lokasi" style={{ color: "white" }} />
            <GooglePlacesAutocomplete
              placeholder="Search"
              minLength={3} // minimum length of text to search
              autoFocus={false}
              returnKeyType={"search"} // Can be left out for default return key https://facebook.github.io/react-native/docs/textinput.html#returnkeytype
              listViewDisplayed="false" // true/false/undefined
              fetchDetails={true}
              renderDescription={row => row.description} // custom description render
              onPress={(data, details = true) => {
                this._onSelectAddress(data);
              }}
              getDefaultValue={() => ""}
              query={{
                // available options: https://developers.google.com/places/web-service/autocomplete
                key: "AIzaSyA1HIexGUBxWEpw6mGrvM1gPIhN-aNuzKY",
                language: "id", // language of the results
                types: "geocode", // default: 'geocode'
                components: "country:ID",
                location: true,
              }}
              styles={{
                textInputContainer: {
                  width: "100%",
                  backgroundColor: "white",
                  borderWidth: 1,
                  borderColor: "#DCE7EF",
                  borderTopLeftRadius: 5,
                  borderTopRightRadius: 5,
                  borderBottomLeftRadius: 5,
                  borderBottomRightRadius: 5,
                  marginVertical: widthPercentageToDP("2%"),
                },
                textInput: {
                  fontFamily: "NunitoSans-Regular",
                  color: "#172344",
                  marginLeft: 0,
                  fontSize: 14,
                  borderWidth: 0,
                },
                description: {
                  fontFamily: "NunitoSans-Black",
                  color: "#172344",
                },
                predefinedPlacesDescription: {
                  color: "#1faadb",
                },
              }}
              currentLocationLabel="Current location"
              nearbyPlacesAPI="GoogleReverseGeocoding" // Which API to use: GoogleReverseGeocoding or GooglePlacesSearch
              debounce={200} // debounce the requests in ms. Set to 0 to remove debounce. By default 0ms.
            />

            <LabelForm style={{color: 'white'}} value="Catatan" />
            <InputForm editable={true} placeholder="Tulis Catatan" value={this.state.nrp} onChangeText={this.onChangeNRP} />

            <LabelForm style={{color: 'white'}} value="Photo" />
            <SAButton title="Foto Selfie" onPress={() => this.selectImageToUpload("photo_selfie")} />
  
            </View>

          </LinearGradient>

          {/* [START Button Submit] */}
          
          <View style={{ padding: widthPercentageToDP("4%") }}>
            <SAButton title="Clock In" onPress={() => this.onPressButton("LiveAttendance")} />
          </View>

          {/* [END Button Submit] */}

        </ScrollView>
      </SafeAreaView>
    );
  }
}

const Notification = props => {
  return (
    <Modal isVisible={props.notifModal}>
      <View
        style={{
          backgroundColor: "white",
          justifyContent: "center",
          alignItems: "center",
          borderRadius: 4,
          borderColor: "rgba(0, 0, 0, 0.1)",
          marginHorizontal: widthPercentageToDP("4%"),
        }}
      >
        <View style={{ paddingVertical: 15, paddingHorizontal: 25 }}>
          <View>
            <View style={{ alignItems: "center", marginBottom: heightPercentageToDP("3%") }}>
              <Text style={{ fontSize: 20, color: "#005596", fontWeight: "600" }}>Sukses</Text>
              <View style={styles.borderTitle} />
            </View>

            <Text
              style={{
                marginBottom: heightPercentageToDP("2.5%"),
                fontFamily: "NunitoSans-SemiBold",
                textAlign: "center",
                fontSize: 16,
                color: "#172344",
              }}
            >
              Booking telah selesai dibuat. Appraiser Toyota Trust akan menghubungi anda.
            </Text>

            <SAButton backgroundColor={"#FB8B34"} title="Oke" onPress={props.notificationToggle} />
          </View>
        </View>
      </View>
    </Modal>
  );
};

const styles = {
  styleLiveClock: {
    fontWeight: 'bold',
    fontSize: RFValue(30),
    color: 'white'
  },
  styleLiveClockStatus: {
    fontWeight: 'bold',
    fontSize: RFValue(30),
    color: '#4F4F4F'
  },
  styleLiveDate: {
    fontSize: RFValue(14),
    color: 'white'
  },
  styleTitleStatus: {
    fontSize: RFValue(14),
    color: '#BDBDBD',
    marginTop: heightPercentageToDP('2%')

  },
  menuBox: {
    paddingHorizontal: widthPercentageToDP("4%"),
    backgroundColor: 'white',
    marginHorizontal: widthPercentageToDP('4%'),
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    marginVertical: heightPercentageToDP('4%'),
    shadowColor: '#000',
    shadowOffset: {
      width: widthPercentageToDP('0%'),
      height: heightPercentageToDP('8%')
    },
    shadowOpacity: 0.44,
    shadowRadius: 10.32,
    elevation: 12,
    alignItems: 'center'
  },
  container: {
    height: "100%",
    backgroundColor: "white",
  },
  mapContainer: {
    marginVertical: widthPercentageToDP("4%"),
  },
  map: {
    width: "100%",
    height: 200,
  },
  dateTimeStyle: {
    marginVertical: widthPercentageToDP("2%"),
    paddingHorizontal: widthPercentageToDP("3%"),
    borderWidth: 1,
    borderColor: "#DCE7EF",
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 5,
  },
  styleTextAttendance: {
    fontSize: RFValue(18),
    color: '#333333',
    fontWeight: 'bold'
  },
  styleTextViewLog: {
    fontSize: RFValue(18),
    color: '#005596'
  },
  title: {
    fontFamily: "NunitoSans-SemiBold",
    fontSize: 24,
    color: "#fff",
  },
  borderTitle: {
    width: widthPercentageToDP("15%"),
    marginTop: heightPercentageToDP("1%"),
    height: 2,
    backgroundColor: "#fff",
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
  },
};

const mapStateToProps = ({ Auth }) => ({ Auth });

export default connect(mapStateToProps)(ClockIn);
