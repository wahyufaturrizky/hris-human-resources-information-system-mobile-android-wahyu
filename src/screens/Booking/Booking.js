import React, { Component } from "react";
import axios from "axios";
import moment from "moment";
import Config from "react-native-config";
import Modal from "react-native-modal";
// import MapView from "react-native-maps";
import DateTimePicker from '@react-native-community/datetimepicker';
import Toast from "react-native-easy-toast";
import "moment/locale/id";

import { View, Text, ScrollView, Dimensions, SafeAreaView,Platform , Button, TouchableOpacity} from "react-native";
import { SAButton, LabelForm, InputForm } from "../../components/Form";
import { GooglePlacesAutocomplete } from "react-native-google-places-autocomplete";
import { connect } from "react-redux";
import { widthPercentageToDP, heightPercentageToDP } from "../../utils";
import TrustIcon from "../../components/TrustIcon/TrustIcon";

moment.locale('id')

class BookingAppraisal extends Component {
  constructor(props) {
    super(props);

    this.state = {
      latMaps: -6.17545698,
      lngMaps: 106.82691335,
      bookingTime: moment().locale("id"),
      yearNow: new Date().getFullYear(),
      carType: "",
      notes: "",
      onRequest: false,
      notifModal: false,
      mode: 'date',
      show: false
    };
    this._onChangeCarType = this._onChangeCarType.bind(this);
    this._onChangeNotes = this._onChangeNotes.bind(this);
    this._onPressSubmitData = this._onPressSubmitData.bind(this);
    this.notificationToggle = this.notificationToggle.bind(this);
  }

  componentDidMount() {
  }

  _onPressSubmitData() {
    const { state } = this;
    const { Auth } = this.props;
    if (!(state.carType && state.notes)) {
      this.refs.toast.show("Semua data wajib di isi");
    } else {
      this.setState(
        {
          onRequest: true,
        },
        () => {
          axios
            .post(
              `${Config.API_URL}/user/${Auth.user.id}/booking`,
              {
                locationLat: state.latMaps,
                locationLng: state.lngMaps,
                bookingTime: state.bookingTime,
                notes: state.notes,
                carType: state.carType,
              },
              {
                headers: { authorization: Auth.token },
              },
            )
            .then(result => {
              this.setState({
                notifModal: true,
              });
            })
            .catch(err => {
              console.log(err);
              this.refs.toast.show("Gagal Booking");
            })
            .finally(() => {
              this.setState({
                onRequest: false,
              });
            });
        },
      );
    }
  }

  _onChangeCarType(value) {
    this.setState({
      carType: value,
    });
  }

  _onChangeNotes(value) {
    this.setState({
      notes: value,
    });
  }

  _changeMarkerByPressMap(coor) {
    this.setState({
      latMaps: coor.coordinate.latitude,
      lngMaps: coor.coordinate.longitude,
    });
  }

  _changeMarkerByAddress(coor) {
    this.map.animateToCoordinate({ latitude: coor.lat, longitude: coor.lng }, 200);
    this.setState({ latMaps: coor.lat, lngMaps: coor.lng });
  }

  _onSelectAddress(place) {
    axios
      .get(
        `https://maps.googleapis.com/maps/api/place/details/json?key=AIzaSyAu5x9OygXYAqRoKC4MF8CUJsGfumPsRUw&placeid=${
          place.place_id
        }&fields=geometry`,
      )
      .then(res => {
        const { result } = res.data;
        this._changeMarkerByAddress(result.geometry.location);
      })
      .catch(err => {
        console.log(err);
      });
  }

  notificationToggle() {
    this.setState(
      {
        notifModal: !this.state.notifModal,
      },
      () => {
        this.props.navigation.navigate("Homepage");
      },
    );
  }

  onChangeDate = (event, selectedDate) => {
    const currentDate = selectedDate || date;
    console.log(this.state.mode, "lili")
    let mode={}
    if(this.state.mode === "date") {
      mode = {
        mode: "time",
        show: true
      }
    }
    this.setState({
      bookingTime: currentDate,
      show: false,
      ...mode
    });
  };

  showDatePicker = currentMode => {
    this.setState({
      show: true,
      mode: currentMode
    });
  };

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <Notification notifModal={this.state.notifModal} notificationToggle={this.notificationToggle} />
        <Toast position="top" positionValue={10} opacity={0.75} ref="toast" />
        <ScrollView>
          <View style={{ padding: widthPercentageToDP("4%") }}>
            <LabelForm value="Tipe Mobil" style={{ color: "#172344" }} />
            <InputForm
              editable={true}
              value={this.state.carType}
              placeholder="contoh: Avanza"
              onChangeText={this._onChangeCarType}
              style={{ borderColor: "#DCE7EF", borderWidth: 1, marginTop: widthPercentageToDP("2%") }}
            />

            <LabelForm value="Pilih Tanggal &amp; waktu" style={{ color: "#172344" }} />
            <View style={{...styles.dateTimeStyle, flex: 1, flexDirection: "row"}}>
              <TouchableOpacity onPress={() => this.showDatePicker("date")}>
                <InputForm
                  editable={false}
                  value={moment(this.state.bookingTime).format("dddd, DD-MM-YYYY HH:mm")}
                  style={{ 
                    borderColor: "white", 
                    borderWidth: 1, 
                    paddingVertical: 0, 
                    backgroundColor: "white",
                    width: widthPercentageToDP("78%"),
                    textAlign: "center",
                  }}
                />
              </TouchableOpacity>
              <TouchableOpacity style={{marginTop: widthPercentageToDP("1%") }} onPress={() => this.showDatePicker("date")}>
                <TrustIcon name={"calendar"} width={30} height={30} />
              </TouchableOpacity>
              {this.state.show && (
                <DateTimePicker
                  testID="dateTimePicker"
                  value={new Date(this.state.bookingTime)}
                  // value={this.state.date}
                  mode={this.state.mode}
                  is24Hour={true}
                  display="default"
                  onChange={this.onChangeDate}
                  maximumDate={new Date(`${this.state.yearNow}-12-31`)}
                  minimumDate={new Date(`${this.state.yearNow}-01-01`)}
                />
              )}
            </View>
            <LabelForm value="Pilih Tempat" style={{ color: "#172344" }} />
            <GooglePlacesAutocomplete
              placeholder="Search"
              minLength={3} // minimum length of text to search
              autoFocus={false}
              returnKeyType={"search"} // Can be left out for default return key https://facebook.github.io/react-native/docs/textinput.html#returnkeytype
              listViewDisplayed="false" // true/false/undefined
              fetchDetails={true}
              renderDescription={row => row.description} // custom description render
              onPress={(data, details = true) => {
                this._onSelectAddress(data);
              }}
              getDefaultValue={() => ""}
              query={{
                // available options: https://developers.google.com/places/web-service/autocomplete
                key: "AIzaSyA1HIexGUBxWEpw6mGrvM1gPIhN-aNuzKY",
                language: "id", // language of the results
                types: "geocode", // default: 'geocode'
                components: "country:ID",
                location: true,
              }}
              styles={{
                textInputContainer: {
                  width: "100%",
                  backgroundColor: "white",
                  borderWidth: 1,
                  borderColor: "#DCE7EF",
                  borderTopLeftRadius: 5,
                  borderTopRightRadius: 5,
                  borderBottomLeftRadius: 5,
                  borderBottomRightRadius: 5,
                  marginVertical: widthPercentageToDP("2%"),
                },
                textInput: {
                  fontFamily: "NunitoSans-Regular",
                  color: "#172344",
                  marginLeft: 0,
                  fontSize: 14,
                  borderWidth: 0,
                },
                description: {
                  fontFamily: "NunitoSans-Black",
                  color: "#172344",
                },
                predefinedPlacesDescription: {
                  color: "#1faadb",
                },
              }}
              currentLocationLabel="Current location"
              nearbyPlacesAPI="GoogleReverseGeocoding" // Which API to use: GoogleReverseGeocoding or GooglePlacesSearch
              debounce={200} // debounce the requests in ms. Set to 0 to remove debounce. By default 0ms.
            />

            {/* <View style={styles.mapContainer}>
              <MapView
                style={styles.map}
                onPress={e => this._changeMarkerByPressMap(e.nativeEvent)}
                ref={ref => {
                  this.map = ref;
                }}
                initialRegion={{
                  latitude: this.state.latMaps,
                  longitude: this.state.lngMaps,
                  latitudeDelta: 0.01,
                  longitudeDelta: 0.01 * (screenWidth / screenHeight),
                }}
              >
                <MapView.Marker coordinate={{ latitude: this.state.latMaps, longitude: this.state.lngMaps }} />
              </MapView>
            </View> */}

            <LabelForm value="Catatan" style={{ color: "#172344" }} />
            <InputForm
              editable={true}
              value={this.state.notes}
              onChangeText={this._onChangeNotes}
              placeholder="Contoh: Sebelah Gang Mangga"
              style={{ borderColor: "#DCE7EF", borderWidth: 1, marginTop: widthPercentageToDP("2%") }}
            />

            <SAButton title="Submit" onPress={this._onPressSubmitData} disabled={this.state.onRequest} />
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const Notification = props => {
  return (
    <Modal isVisible={props.notifModal}>
      <View
        style={{
          backgroundColor: "white",
          justifyContent: "center",
          alignItems: "center",
          borderRadius: 4,
          borderColor: "rgba(0, 0, 0, 0.1)",
          marginHorizontal: widthPercentageToDP("4%"),
        }}
      >
        <View style={{ paddingVertical: 15, paddingHorizontal: 25 }}>
          <View>
            <View style={{ alignItems: "center", marginBottom: heightPercentageToDP("3%") }}>
              <Text style={{ fontSize: 20, color: "#005596", fontWeight: "600" }}>Sukses</Text>
              <View style={styles.borderTitle} />
            </View>

            <Text
              style={{
                marginBottom: heightPercentageToDP("2.5%"),
                fontFamily: "NunitoSans-SemiBold",
                textAlign: "center",
                fontSize: 16,
                color: "#172344",
              }}
            >
              Booking telah selesai dibuat. Appraiser Toyota Trust akan menghubungi anda.
            </Text>

            <SAButton backgroundColor={"#FB8B34"} title="Oke" onPress={props.notificationToggle} />
          </View>
        </View>
      </View>
    </Modal>
  );
};

const styles = {
  container: {
    height: "100%",
    backgroundColor: "white",
  },
  mapContainer: {
    marginVertical: widthPercentageToDP("4%"),
  },
  map: {
    width: "100%",
    height: 200,
  },
  dateTimeStyle: {
    marginVertical: widthPercentageToDP("2%"),
    paddingHorizontal: widthPercentageToDP("3%"),
    borderWidth: 1,
    borderColor: "#DCE7EF",
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 5,
  },
  borderTitle: {
    width: widthPercentageToDP("15%"),
    marginTop: heightPercentageToDP("1%"),
    height: 2,
    backgroundColor: "#FB8B34",
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
  },
};

const mapStateToProps = ({ Auth }) => ({ Auth });

export default connect(mapStateToProps)(BookingAppraisal);
