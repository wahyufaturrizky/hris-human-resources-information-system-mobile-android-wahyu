import React, { Component } from "react";
import moment from "moment";
import { Text, View, ScrollView, TouchableOpacity, Animated } from "react-native";
import DateTimePicker from '@react-native-community/datetimepicker';
import { widthPercentageToDP } from "../../utils";
import TrustIcon from "../../components/TrustIcon";
import {axios} from "../../utils";
import Toast from "react-native-easy-toast";
import { InputForm } from "../../components/Form";
moment.locale("id")

class AppraisalDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      bookingTime: null,
      booking: {},
      show: false,
      mode: 'date',
      bookingTime: moment().locale("id"),
      yearNow: new Date().getFullYear(),
    };
  }

  UNSAFE_componentWillMount () {
    const { booking } = this.props.route.params;
    this.setState({...booking})
  }

  colorStatusMap = status => {
    switch (status.trim()) {
      case "Selesai":
        return "green";
      case "Dalam Proses":
        return "#f2c900";
      case "Tidak Dilanjutkan":
        return "red";
      case "Rejected":
        return "red";
      case "Approved":
        return "green";
      default:
        return "#f2c900";
    }
  };

  onChangeDate = (event, selectedDate) => {
    const currentDate = selectedDate || this.state.date;
    if(this.state.mode === "date") {
      this.setState({
        bookingTime: currentDate,
        show: true,
        mode: "time"
      });
    } else {
      axios
      .put(`/booking/${this.state.id}/reschedule-booking`, {bookingTime: currentDate})
      .then( res => {
        this.setState({
          ...this.state,
          ...res.data.booking,
          show: false,
        })
        this.props.navigation.navigate('StatusAppraisal', {booking: res.data.booking })
        this.refs.toast.show(`update booking with booking number ${res.data.booking.noBooking}`, 2000, () => {});
      })
      .catch(e => {
        this.refs.toast.show(e.message);
      })  
    }
  };

  showDatePicker (currentMode) {
    this.setState({
      show: true,
      mode: currentMode
    });
  };

  render() {
    const {id, Appraisal, BookingStatuses, bookingTime, show} = this.state
    return (
      <View style={styles.container}>
        <ScrollView style={styles.listContainer}>
          <Toast position="top" positionValue={70} opacity={0.75} ref="toast" />
          <View style={styles.infoArea}>
            <View style={styles.infoDirect}>
              <View style={styles.infoRow}>
                <View style={styles.infoLabel}>
                  <Text style={{ fontFamily: "NunitoSans-SemiBold", color: "#172344", }}>Booking ID</Text>
                </View>
                <View style={styles.infoValue}>
                  <Text style={{fontFamily: "NunitoSans-SemiBold", fontSize: 16, color: "#005596"}}>#{id}</Text>
                </View>
              </View>
  
              <View style={styles.infoRow}>
                <View style={styles.infoLabel}>
                  <Text style={{  fontFamily: "NunitoSans-SemiBold", color: "#172344", }}>Plat Kendaraan</Text>
                </View>
                <View style={styles.infoValue}>
                  <Text style={{fontFamily: "NunitoSans-SemiBold", fontSize: 16, color: "#005596"}}>{Appraisal ? Appraisal.carDetail[7].value.toUpperCase() : null}</Text>
                </View>
              </View>
  
              <View style={styles.infoRow}>
                <View style={styles.infoLabel}>
                  <Text style={{  fontFamily: "NunitoSans-SemiBold", color: "#172344", }}>Status Terakhir</Text>
                </View>
                <View style={styles.infoValue}>
                  <Text style={{ fontFamily: "NunitoSans-Black", fontSize: 15, flex: 1, flexWrap: 'wrap', color: this.colorStatusMap(
                      BookingStatuses[0].status,
                    ) }}>
                    {`${BookingStatuses[0].status_label} - ${BookingStatuses[0].status}`}
                  </Text>
                </View>
              </View>
  
              <View style={BookingStatuses[0].status_label === "Appraisal" && BookingStatuses[0].status === "Dalam Proses" ? {} : styles.infoRow}>
                <View style={styles.infoLabel}>
                  <Text style={{  fontFamily: "NunitoSans-SemiBold", color: "#172344", }}>Waktu Booking</Text>
                </View>
                <View style={styles.infoValue}>
                  { BookingStatuses[0].status_label === "Appraisal" && BookingStatuses[0].status === "Dalam Proses"
                  ? (
                    <View style={{...styles.dateTimeStyle, flex: 1, flexDirection: "row"}}>
                      <TouchableOpacity onPress={() => this.showDatePicker("date")}>
                        <InputForm
                          editable={false}
                          value={moment(bookingTime).format("dddd, DD-MM-YYYY HH:mm")}
                          style={{ 
                            borderColor: "white", 
                            borderWidth: 1, 
                            paddingVertical: 0, 
                            backgroundColor: "white",
                            width: widthPercentageToDP("50%"),
                            textAlign: "center",
                          }}
                        />
                      </TouchableOpacity>
                      <TouchableOpacity style={{marginTop: widthPercentageToDP("1%") }} onPress={() => this.showDatePicker("date")}>
                        <TrustIcon name={"calendar"} width={30} height={30} />
                      </TouchableOpacity>
                      {this.state.show && (
                        <DateTimePicker
                          testID="dateTimePicker"
                          value={new Date(this.state.bookingTime)}
                          mode={this.state.mode}
                          is24Hour={true}
                          display="default"
                          onChange={(event, selected) => this.onChangeDate(event, selected)}
                          maximumDate={new Date(`${this.state.yearNow}-12-31`)}
                          minimumDate={new Date(`${this.state.yearNow}-01-01`)}
                        />
                      )} 
                    </View>
                  ) : (
                    <Text style={{ fontFamily: "NunitoSans-SemiBold", color: "#172344" }}>
                    {`${moment(bookingTime).format("dddd, Do MMM YYYY")} Pukul ${moment(bookingTime).format("HH.mm")}`}
                    </Text>
                  )}
                </View>
              </View>
              
            </View>
          </View>
  
          <View>
            {BookingStatuses.map((status, index) => (
              <View key={index} style={styles.statusBox}>
                <View style={{justifyContent: "center"}}>
                  <TrustIcon name="success" width={25} height={25} />
                </View>
  
                <View style={{marginLeft: widthPercentageToDP("4%")}}>
                  {
                    status.status_label === "Appraisal"
                    ? (
                      <Text>{`${moment(bookingTime).format("dddd, Do MMM YYYY")} Pukul ${moment(bookingTime).format("HH.mm")}`}</Text>
                    ) : (
                      <Text>{`${moment(status.createdAt).format("dddd, Do MMM YYYY")} Pukul ${moment(status.createdAt).format("HH.mm")}`}</Text>
                    )
                  }
                  <Text style={{ fontFamily: "NunitoSans-Black", fontSize: 16, color: this.colorStatusMap(
                      status.status,
                    ) }}>
                    {`${status.status_label} - ${status.status}`}
                  </Text>
                </View>
              </View>
            ))}
          </View>
  
        </ScrollView>
      </View>
    );
  }
}

const styles = {
  container: {
    height: "100%",
    backgroundColor: "white",
  },
  listContainer: {
    padding: widthPercentageToDP("4%"),
  },
  infoArea: {
    flexDirection: "row",
    backgroundColor: "#EEF2F5",
    marginBottom: widthPercentageToDP("4%"),
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 5,
    paddingHorizontal: widthPercentageToDP("4%"),
    paddingVertical: widthPercentageToDP("2%"),
  },
  statusBox: {
    flexDirection: "row",
    marginVertical: widthPercentageToDP("1%"),
    padding: widthPercentageToDP("4%"),
    borderWidth: 1,
    borderColor: "#9EBED7",
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 5,
  },
  dateTimeStyle: {
    marginVertical: widthPercentageToDP("2%"),
    // paddingHorizontal: widthPercentageToDP("3%"),
    backgroundColor: "white",
    borderWidth: 1,
    borderColor: "#DCE7EF",
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 5,
    width: widthPercentageToDP("63%")
  },
  infoDirect: {flex: 1, flexDirection: "column"},
  infoRow: {flex: 1, flexDirection: "row"},
  infoLabel: {
    width: widthPercentageToDP("30%")
  },
  infoValue: {
    width: widthPercentageToDP("60%")
  }
};

export default AppraisalDetail;
