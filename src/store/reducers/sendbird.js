const defaultState = {
  error: "",
  user: null,
}

export default (state = defaultState, action) => {
  switch(action.type) {
    case "CONNECT_SUCCESS" :
      return { ...state, user: action.payload};

    case "CONNECT_FAILED" :
      return { ...state, error: action.error};

    case "DISCONNECT_SUCCESS" :
      return { ...state, isDisconnected: ture};

    case "DISCONNECT_FAILED" :
      return { ...state, error: action.error };

    case "FETCH_MESSAGES_SUCCESS" :
      return { ...state, messages: action.payload};

    case "FETCH_MESSAGES_FAILED" :
      return { ...state, error: action.error };

    case "SEND_MESSAGE_SUCCESS" :
      return { ...state, message: action.payload};

    case "SEND_MESSAGE_FAILED" :
      return { ...state, error: action.error };

    default:
      return state;
  }
}
