import { AsyncStorage } from "react-native";
const defaultState = { isAuthenticated: false, onBoardVisited: false };
export default (state = defaultState, action) => {
  switch (action.type) {
    case "AUTH_REQUEST":
      return { isAuthenticated: false, authRequest: true };
    case "AUTH_REQUEST_FAILED":
      return { isAuthenticated: false, error: action.payload.message };
    case "AUTH_REQUEST_SUCCESS":
      return { isAuthenticated: true, ...action.payload };
    case "AUTH_LOGOUT":
      return { ...state, token: null, isAuthenticated: false, isLogout: true };
    case "ONBOARD_VISITED": 
      return {...state, onBoardVisited: true}
        default:
      return state;
  }
};
