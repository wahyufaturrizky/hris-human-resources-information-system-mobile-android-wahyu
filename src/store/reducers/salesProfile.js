const defaultState = {branches: [], supervisors:[], branch_heads: [],};
export default (state = defaultState, action) => {
  switch (action.type) {
    case "REQ_SALES_PROFILE":
      return { ...state, onRequest: true };
    case "SET_SALES_PROFILE":
      return { ...state, ...action.payload };
    default:
      return state;
  }
};
