import 'react-native-gesture-handler';
import * as React from 'react';
import { Provider } from "react-redux";
import TrustIcon from "./components/TrustIcon/TrustIcon";
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator, HeaderBackButton } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import AsyncStorage from '@react-native-community/async-storage';
import {Icon} from 'react-native-elements';

// screens
import store from "./store/configureStore";
import HomepageScreen from "./screens/Homepage/Homepage";
import CodeOTPScreen from "./screens/CodeOTP/CodeOTP";
import LoginScreen from "./screens/Login/Login";
import SignUpScreen from "./screens/SignUp/SignUp";
import ProfileScreen from "./screens/Profile/Profile";
import BookingScreen from "./screens/Booking/Booking";
import LiveAttendanceScreen from "./screens/LiveAttendance/LiveAttendance";
import ClockInScreen from "./screens/ClockIn/ClockIn";
import ClockOutScreen from "./screens/ClockOut/ClockOut";
import AppraisalScreen from "./screens/StatusAppraisal/StatusAppraisal";
import LogoutScreen from "./screens/Logout/Logout";
import PricingCalculatorScreen from "./screens/PricingCalculator/PricingCalculator";
import PricingCalculatorResultScreen from "./screens/PricingCalculatorResult/PricingCalculatorResult";
import AppraisalDetailScreen from "./screens/AppraisalDetail/AppraisalDetail";
import UnderConstructionScreen from "./screens/UnderConstruction/UnderConstruction";
import OnBoardingScreen from "./screens/OnBoarding/OnBoarding";
import ReportScreen from './screens/Report/Report';
import { widthPercentageToDP, heightPercentageToDP, versionCompare } from "./utils";

const HomeStack = createStackNavigator();
const AppraisalStack = createStackNavigator();
const HomeTabs = createBottomTabNavigator();

const AppraisalNavigatorScreens = () => (
  <AppraisalStack.Navigator initialRouteName="StatusAppraisal">
    <HomeStack.Screen
      name="StatusAppraisal"
      component={AppraisalScreen}
      options={({ navigation }) => ({
        title: "Status Appraisal",
        headerLeft: () => (
          <Icon name="arrowleft" style={{paddingLeft:10}} color="#fff" type="antdesign" onPress={() => navigation.navigate("Homepage")} />
        ),
        headerStyle: {
          backgroundColor: "#005596",
        },
        headerTintColor: "#fff",
        headerTitleStyle: {
          fontFamily: "NunitoSans-Black",
        },
      })}
    />
    <HomeStack.Screen
    name="AppraisalDetail"
    component={AppraisalDetailScreen}
    options={({ navigation }) => ({
      title: "Detail Appraisal",
      headerStyle: {
        backgroundColor: "#005596",
      },
      headerTintColor: "#fff",
      headerTitleStyle: {
        fontFamily: "NunitoSans-Black",
      },
    })}
    />
  </AppraisalStack.Navigator>
)

const HomeScreens = ({ navigation }) => (
  <HomeStack.Navigator 
    initialRouteName="Homepage"
  >
    <HomeStack.Screen 
      name="Homepage" 
      component={HomeTabScreens}
      options={{
        headerShown: false,
      }}
    />
    <HomeStack.Screen 
      name="PricingCalculator" 
      component={PricingCalculatorScreen}
      options={{
        title: "Pricing Calculator",
        headerStyle: {
          backgroundColor: "#005596",
        },
        headerTintColor: "#fff",
        headerTitleStyle: {
          fontFamily: "NunitoSans-Black",
        },
      }}
    />
    <HomeStack.Screen
      name="PricingCalculatorResult"
      component={PricingCalculatorResultScreen}
      options={{
        title: "Pricing Result",
        headerStyle: {
          backgroundColor: "#005596",
        },
        headerTintColor: "#fff",
        headerTitleStyle: {
          fontFamily: "NunitoSans-Black",
        },
      }}
    />
    <HomeStack.Screen
      name="Appraisal"
      component={BookingScreen}
      options={{
        title: "Book Appraisal",
        headerStyle: {
          backgroundColor: "#005596",
        },
        headerTintColor: "#fff",
        headerTitleStyle: {
          fontFamily: "NunitoSans-Black",
        },
      }}
    />
    <HomeStack.Screen
      name="LiveAttendance"
      component={LiveAttendanceScreen}
      options={{
        title: "Live Attendance",
        headerStyle: {
          backgroundColor: "#45B7FB",
        },
        headerTintColor: "#fff",
        headerTitleStyle: {
          fontFamily: "NunitoSans-Black",
        },
      }}
    />
    <HomeStack.Screen
      name="ClockIn"
      component={ClockInScreen}
      options={{
        title: "Clock In",
        headerStyle: {
          backgroundColor: "#45B7FB",
        },
        headerTintColor: "#fff",
        headerTitleStyle: {
          fontFamily: "NunitoSans-Black",
        },
      }}
    />
    <HomeStack.Screen
      name="ClockOut"
      component={ClockOutScreen}
      options={{
        title: "Clock Out",
        headerStyle: {
          backgroundColor: "#45B7FB",
        },
        headerTintColor: "#fff",
        headerTitleStyle: {
          fontFamily: "NunitoSans-Black",
        },
      }}
    />
    <HomeStack.Screen
      name="StatusAppraisal"
      component={AppraisalNavigatorScreens}
      options={{
        header: null,
        headerShown: false
      }}
    />
    <HomeStack.Screen
      name="Report"
      component={ReportScreen}
      options={{
        title: "Report",
        headerStyle: {
          backgroundColor: "#005596",
        },
        headerTintColor: "#fff",
        headerTitleStyle: {
          fontFamily: "NunitoSans-Black",
        },
      }}
    />
    <HomeStack.Screen
      name="UnderConstruction"
      component={UnderConstructionScreen}
      options={{
        title: "Sedang Dalam Pembuatan",
        headerStyle: {
          backgroundColor: "#45B7FB",
        },
        headerTintColor: "#fff",
        headerTitleStyle: {
          fontFamily: "NunitoSans-Black",
        },
      }}
    />
    <HomeStack.Screen
      name="Logout"
      component={LogoutScreen}
      options={{
        headerShown: false
      }}
    />
  </HomeStack.Navigator>
)

const HomeTabScreens = () => (
  <HomeTabs.Navigator
    screenOptions= {({ route, navigation }) => ({
      tabBarIcon: ({ focused }) => {
        let iconName;

        if (route.name === "Home") {
          iconName = `beranda${focused ? "" : "_1"}`;
        } else if (route.name === "Profil") {
          iconName = `profil${focused ? "" : "_1"}`;
        } else if (route.name === "Inbox") {
          iconName = `inbox${focused ? "" : "_1"}`;
        } else if (route.name === "Aktivitas") {
          iconName = `aktivitas${focused ? "" : "_1"}`;
        }

        return <TrustIcon name={iconName} width={25} height={25} />;
      },
    })}
    tabBarOptions= {{
      activeTintColor: "#005596",
      activeBackgroundColor: "white",
      inactiveTintColor: "#BDBEC0",
      style: {
        elevation: 10,
        borderTopColor: "#DCE7EF",
        backgroundColor: "white",
        height: 60,
      },
    }}
  >
    <HomeTabs.Screen name="Home" component={HomepageScreen} />
    {/* <HomeTabs.Screen name="Inbox" component={UnderConstructionScreen} /> */}
    <HomeTabs.Screen 
      name="Aktivitas" 
      component={UnderConstructionScreen} 
    />
    <HomeTabs.Screen 
      name="Profil" 
      // component={ProfileScreen}
      component={UnderConstructionScreen}
    />
  </HomeTabs.Navigator>
)


const Stack = createStackNavigator();
const LoginStackScreen = () => (
  <Stack.Navigator
    initialRouteName={"onBoarding"}
    screenOptions={{
      // headerShown: true
      headerShown: false
    }}
  >
    <Stack.Screen name="onBoarding" component={OnBoardingScreen} />
    <Stack.Screen name="Login" component={LoginScreen} />
    <Stack.Screen name="SignUp" component={SignUpScreen} />
    <Stack.Screen name="CodeOTP" component={CodeOTPScreen} />
    <Stack.Screen name="Mainpage" component={HomeScreens} />
  </Stack.Navigator>
)

const Navigation = () => {
  const [isOnBoardVisited, setOnBoard] = React.useState(false);
  const [userDetail, setUserDetail] = React.useState(null);

  React.useEffect(() => {
    const bootstrapAsync = async () => {
      let userToken;
      let onBoardVisited;

      try {
        userToken = await AsyncStorage.getItem("userDetail");
        onBoardVisited = await AsyncStorage.getItem("onBoardFinished");
        userToken = JSON.parse(userToken);
      } catch (e) {
        console.log(e)
      }
      setOnBoard(onBoardVisited === "true");
      setUserDetail(userToken ? userToken.token : null)
    };

    bootstrapAsync();
  }, []);

  return (
    <Provider store={store}>
      <NavigationContainer>
        <LoginStackScreen/>
      </NavigationContainer>
    </Provider>
  )
};

export default Navigation;