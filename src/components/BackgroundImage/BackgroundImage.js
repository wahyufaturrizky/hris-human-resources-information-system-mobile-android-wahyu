import React, { useState, useEffect } from 'react';
import { ImageBackground } from "react-native";
import AnimatedSplash from "../../../lib/AnimatedSplash";

const BackgroundImage = (props) => {
  const [isLoaded, setIsLoaded] = useState(false);

  useEffect(() => {
    setTimeout(() => {
      setIsLoaded(true);
    }, 1000);
  }, []);
    return (
      <>
        <AnimatedSplash
          logoWidht={150}
          logoHeight={150}
          isLoaded={isLoaded}
          backgroundColor={"#4A9DDB"}
          logoImage={require("../../assets/images/hris_logo_white.png")}>
          <ImageBackground source={require('../../assets/images/background_white.png')} style={styles.backgroundImage}>
            {props.children}
          </ImageBackground>
        </AnimatedSplash>
      </>
    )
}

let styles = {
  backgroundImage: {
    flex: 1,
    width: null,
    height: null,
    resizeMode: 'cover'
  }
}
export default BackgroundImage;