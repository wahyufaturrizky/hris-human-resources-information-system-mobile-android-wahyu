/* eslint-disable react-native/no-inline-styles */
/* eslint-disable no-dupe-keys */
import React, {Component} from 'react';
import {Text, View, TouchableOpacity, Image} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

export default class ComponentMonthPeriod extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedMonth: false,
    };
  }
  toggleSelectMonth = () => {
    this.setState({selectedMonth: !this.state.selectedMonth});
  };
  render() {
    const {valueMonth} = this.props;
    return (
      <TouchableOpacity style={{
                    width: '14%',
                    marginHorizontal: '1%',
                    marginBottom: RFValue(10),
                    shadowOpacity: 1,
                    elevation: 2,
                    height: undefined,
                    aspectRatio: 1,
                    justifyContent: 'center',
                    alignItems: 'center',
                    backgroundColor: 'white',
                    elevation: 2,
                    borderRadius: RFValue(10),
                  }} onPress={this.toggleSelectMonth}>
        {this.state.selectedMonth ? (
        <View style={{
                        alignItems: 'center',
                        height: '100%',
                        width: '100%',
                        justifyContent: 'center',
                        backgroundColor: 'rgba(74, 157, 219, 0.5)',
                        borderColor: 'rgba(74, 157, 219, 1)',
                        borderWidth: 2,
                        borderRadius: RFValue(10),
                      }}>
          <Text style={{fontSize: RFValue(16), color: "#4A9DDB"}}>{valueMonth}</Text>
        </View>
        ) : (
        <View style={{
                      alignItems: 'center',
                      height: '100%',
                      width: '100%',
                      justifyContent: 'center',
                      backgroundColor: 'rgba(157, 163, 180, 0.10)',
                      borderRadius: RFValue(10),
                    }}>
          <Text style={{fontSize: RFValue(16), color:"#828282"}}>{valueMonth}</Text>
        </View>
        )}
      </TouchableOpacity>
    );
  }
}
