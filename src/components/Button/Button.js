import React from "react";
import { TouchableOpacity, View, Text } from "react-native";

const style = {
  button: {
    backgroundColor: "#FB3434",
    borderRadius: 5,
    width: 80,
    height: 30,
    justifyContent: "center",
    alignItems: "center",
  },
  text: {
    color: "#fff",
    fontSize: 12,
  },
};

const Button = ({ onPress, text, textStyle, buttonStyle }) => (
  <TouchableOpacity onPress={onPress}>
    <View style={{ ...style.button, ...buttonStyle }}>
      <Text style={{ ...style.text, ...textStyle }}>{text}</Text>
    </View>
  </TouchableOpacity>
);

export default Button;
